package ru.gowk.android.sslsocketfactory;

import javax.net.ssl.*;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * Created on Apr 13, 2016
 * @author Vyacheslav Gorbatykh
 */
public class TlsSocketFactory extends SSLSocketFactory {

    private static final String[] ENABLED_PROTOCOLS = new String[]{"TLSv1", "TLSv1.1", "TLSv1.2"};

    private final SSLSocketFactory delegate;

    public TlsSocketFactory() throws NoSuchAlgorithmException, KeyManagementException {
        SSLContext context = SSLContext.getInstance("TLS");
        context.init(null, new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] chain, String authType)
                            throws CertificateException {

                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] chain, String authType)
                            throws CertificateException {

                    }

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                }
        }, null);
        delegate = context.getSocketFactory();
    }

    @Override
    public String[] getDefaultCipherSuites() {
        return delegate.getDefaultCipherSuites();
    }

    @Override
    public String[] getSupportedCipherSuites() {
        return delegate.getSupportedCipherSuites();
    }

    @Override
    public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
        return enableTls(delegate.createSocket(s, host, port, autoClose));
    }

    @Override
    public Socket createSocket(String host, int port) throws IOException {
        return enableTls(delegate.createSocket(host, port));
    }

    @Override
    public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException {
        return enableTls(delegate.createSocket(host, port, localHost, localPort));
    }

    @Override
    public Socket createSocket(InetAddress host, int port) throws IOException {
        return enableTls(delegate.createSocket(host, port));
    }

    @Override
    public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
        return enableTls(delegate.createSocket(address, port, localAddress, localPort));
    }

    private static Socket enableTls(Socket socket) {
        if (socket instanceof SSLSocket) {
            SSLSocket sslSocket = (SSLSocket) socket;

            String[] enabledProtocols = sslSocket.getEnabledProtocols();
            String[] q= new String[enabledProtocols.length + ENABLED_PROTOCOLS.length];
            System.arraycopy(enabledProtocols, 0, q, 0, enabledProtocols.length);
            System.arraycopy(ENABLED_PROTOCOLS, 0, q, enabledProtocols.length, ENABLED_PROTOCOLS.length);

            sslSocket.setEnabledProtocols(q);
        }
        return socket;
    }
}
