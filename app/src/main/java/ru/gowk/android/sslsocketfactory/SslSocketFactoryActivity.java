package ru.gowk.android.sslsocketfactory;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import ru.gowk.android.R;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created on Jun 02, 2016
 * @author Vyacheslav Gorbatykh
 */
public class SslSocketFactoryActivity extends Activity {

    private static SSLSocketFactory defaultSSLSocketFactory = HttpsURLConnection.getDefaultSSLSocketFactory();

    private TlsSocketFactory socketFactory;

    private EditText urlEdit;
    private CheckBox useCustomCheck;
    private TextView textView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sslsocketfactory);

        urlEdit = (EditText) findViewById(R.id.urlEdit);
        useCustomCheck = (CheckBox) findViewById(R.id.useCustom);

        textView = (TextView) findViewById(R.id.textView);
        textView.setText("Click button");

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final String url = urlEdit.getText().toString();
                final boolean useCustom = useCustomCheck.isChecked();
                new Thread() {
                    public void run() {
                        testConnection(url, useCustom);
                    }
                }.start();
            }
        });


        try {
            socketFactory = new TlsSocketFactory();
        } catch (Exception e) {
            Log.e("sslsocketfactory", "fail to create socket factory", e);
            textView.setText("fail to create socket factory");
        }

        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });
    }

    private void testConnection(String url, boolean useCustom) {
        Log.v("sslsocketfactory", "----default ssl socket factory == system----");
        HttpsURLConnection.setDefaultSSLSocketFactory(defaultSSLSocketFactory);
        connect(url, useCustom);

        Log.v("sslsocketfactory", "----default ssl socket factory == custom----");
        HttpsURLConnection.setDefaultSSLSocketFactory(socketFactory);
        connect(url, useCustom);
    }

    private void connect(String url, boolean useCustom) {
        Log.v("sslsocketfactory", "start[" + url + "]");
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setConnectTimeout(15000);
            conn.setReadTimeout(15000);

            if (useCustom && conn instanceof HttpsURLConnection) {
                Log.v("sslsocketfactory", "set custom ssl socket factory");
                ((HttpsURLConnection) conn).setSSLSocketFactory(socketFactory);
            }

            int responseCode = conn.getResponseCode();

            byte[] q = new byte[2048];
            int read = conn.getInputStream().read(q);
            String text = new String(q, 0, read);

            Log.v("sslsocketfactory", "complete[" + url + "]: (" + responseCode + ") " + text.substring(0, 256));
        } catch (Exception e) {
            Log.v("sslsocketfactory", "exception[" + url + "]: " + e.getMessage(), e);
        }
    }
}
