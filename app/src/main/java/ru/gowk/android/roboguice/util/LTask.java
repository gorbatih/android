package ru.gowk.android.roboguice.util;

import ru.gowk.android.App;

import javax.inject.Inject;
import java.util.Random;

/**
 * @author slavca
 * @since 13.05.2016
 */
public abstract class LTask {
    @Inject
    private Random random;

    public void execute() {
        App.INJECTOR.injectMembers(this);

        new Thread() {
            @Override
            public void run() {
                doInBackground();
            }
        }.start();
    }

    protected abstract void doInBackground();

    protected String getNext() {
        return String.valueOf(random.nextInt(100));
    }
}
