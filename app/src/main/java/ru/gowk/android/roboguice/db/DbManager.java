package ru.gowk.android.roboguice.db;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Random;

/**
 * @author slavca
 * @since 04.05.2016
 */
@Singleton
public class DbManager {
    @Inject
    private ArrayList<String> strList;
    @Inject
    private Random random;

    private boolean init = true;

    public String get() {
        if (init) {
            init();
        }
        return strList.get(random.nextInt(strList.size()));
    }

    private void init() {
        strList.add("first");
        strList.add("second");
        strList.add("third");
        init = false;
    }
}
