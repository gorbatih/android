package ru.gowk.android.roboguice;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.TextView;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import ru.gowk.android.MainApplication;
import ru.gowk.android.R;
import ru.gowk.android.roboguice.db.DbManager;
import ru.gowk.android.roboguice.util.LTask;

import javax.inject.Inject;

@ContentView(R.layout.activity_main)
public class RoboGuiceActivity extends RoboActivity {

    @Inject
    private DbManager dbManager;
    @InjectView(R.id.mainTextView)
    private TextView mainTextView;
    @InjectView(R.id.textView)
    private TextView textView;
    @InjectView(R.id.webview)
    private WebView webView;

    @Override
    protected void onResume() {
        super.onResume();

        mainTextView.setText("" + (System.currentTimeMillis() - MainApplication.startTime));


        new A().execute();

        doSome();
    }

    private static void doSome() {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException ignored) {
        }

        new LTask() {
            @Inject
            private DbManager dbManager;

            @Override
            protected void doInBackground() {
                try {
                    Thread.sleep(4000);
                    Log.i("sl-test", "Test anon toast: " + dbManager.get());
                } catch (Throwable t) {
                    Log.e("sl-test", "anon failed", t);
                }
            }
        }.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private static class A extends LTask {
        @Inject
        private DbManager dbManager;

        @Override
        protected void doInBackground() {
            try {
                Thread.sleep(5000);
                Log.i("sl-test", "Test toast: " + dbManager.get());
            } catch (Throwable t) {
                Log.e("sl-test", "failed", t);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
