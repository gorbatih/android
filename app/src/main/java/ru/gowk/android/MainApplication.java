package ru.gowk.android;

import android.app.Application;
//import roboguice.RoboGuice;

/**
 * @author slavca
 * @since 04.05.2016
 */
public class MainApplication extends Application {
    public static final long startTime = System.currentTimeMillis();

    @Override
    public void onCreate() {
        App.init(this);
        super.onCreate();
    }

//    public MainApplication() {
//        RoboGuice.setUseAnnotationDatabases(false);
//    }
}
