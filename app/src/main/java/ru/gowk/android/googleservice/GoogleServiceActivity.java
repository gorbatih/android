package ru.gowk.android.googleservice;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.security.ProviderInstaller;
import ru.gowk.android.R;

/**
 * Created on Jun 03, 2016
 * @author Vyacheslav Gorbatykh
 */
public class GoogleServiceActivity extends Activity {

    private TextView textView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.button_text);

        textView = (TextView) findViewById(R.id.textView);
        textView.setText("Click button to call \"ProviderInstaller.installIfNeeded\"");

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Log.e("googleservice", "before installIfNeeded");
                    ProviderInstaller.installIfNeeded(GoogleServiceActivity.this);
                } catch (GooglePlayServicesRepairableException e) {
                    Log.e("googleservice", "Got GooglePlayServicesRepairableException(" + e.getConnectionStatusCode() + ", \"" + e.getMessage() + "\"), show a dialog...", e);
                    textView.setText("Got GooglePlayServicesRepairableException(" + e.getConnectionStatusCode() + ", \"" + e.getMessage() + "\"), show a dialog...");

                    int connectionStatusCode = e.getConnectionStatusCode();
                    GooglePlayServicesUtil.showErrorNotification(connectionStatusCode, GoogleServiceActivity.this);
                    Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(connectionStatusCode, GoogleServiceActivity.this, 1);
                    errorDialog.show();

                } catch (GooglePlayServicesNotAvailableException e) {
                    Log.e("googleservice", "Got GooglePlayServicesNotAvailableException(\"" + e.getMessage() + "\")", e);
                    textView.setText("Got GooglePlayServicesNotAvailableException(\"" + e.getMessage() + "\")");
                }
            }
        });
    }
}
