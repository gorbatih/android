package ru.gowk.android.fragment;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import ru.gowk.android.R;

/**
 * Created on Jul 01, 2016
 * @author Vyacheslav Gorbatykh
 */
public class Fragment2 extends BaseFragment {

    public Fragment2() {
        super("slavca-fragment2");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        log("onCreateView", "savedInstanceState", savedInstanceState == null ? "null" : "not null");
        return inflater.inflate(R.layout.fragment2, null);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        log("onCreate", "savedInstanceState", savedInstanceState == null ? "null" : "not null");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        log("onViewCreated", "savedInstanceState", savedInstanceState == null ? "null" : "not null");
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        log("onStart");
        super.onStart();
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        log("setUserVisibleHint", "isVisibleToUser", isVisibleToUser);
    }

    @Override
    public void onResume() {
        log("onResume");
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        log("onSaveInstanceState");
        super.onSaveInstanceState(outState);

        outState.putInt("fake", 0);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        log("onConfigurationChanged");
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onPause() {
        log("onPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        log("onStop");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        log("onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        log("onDestroy");
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        log("onDetach");
        super.onDetach();
    }
}
