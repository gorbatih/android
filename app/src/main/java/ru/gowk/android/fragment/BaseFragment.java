package ru.gowk.android.fragment;

import android.app.Fragment;
import android.util.Log;

/**
 * Created on Jul 26, 2016
 *
 * @author Vyacheslav Gorbatykh
 */
abstract class BaseFragment extends Fragment {
    protected final String prefix;

    BaseFragment(String prefix) {
        this.prefix = prefix;
    }

    void log(String name, Object... objects) {
        StringBuilder builder = new StringBuilder()
                .append('[').append(hashCode()).append("] ").append(name);
        if (objects != null) {
            for (int i = 0; (i + 1) < objects.length; i += 2) {
                builder.append(", ").append(objects[i]).append('=').append(objects[i + 1]);
            }
        }
        Log.i(prefix, builder.toString());
    }
}
