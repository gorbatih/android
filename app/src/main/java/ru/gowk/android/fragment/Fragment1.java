package ru.gowk.android.fragment;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import ru.gowk.android.R;

import java.io.File;
import java.util.Random;

/**
 * Created on Jul 01, 2016
 * @author Vyacheslav Gorbatykh
 */
public class Fragment1 extends BaseFragment {

    private File file;
    private static final Random random = new Random();

    public Fragment1() {
        super("slavca-fragment1");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        log("onCreateView",
                "savedInstanceState", savedInstanceState == null ? "null" : "not null",
                "file", file);
        return inflater.inflate(R.layout.fragment1, null);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        log("onCreate",
                "savedInstanceState", savedInstanceState == null ? "null" : "not null",
                "file", file);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        boolean isNewFile = false;
        if (file == null) {
            if (savedInstanceState == null) {
                file = new File("a" + random.nextInt() + ".txt");
                isNewFile = true;
            } else {
                file = (File) savedInstanceState.getSerializable("file");
            }
        }

        log("onViewCreated",
                "savedInstanceState", savedInstanceState == null ? "null" : "not null",
                "file", file, isNewFile ? " (new)" : "");
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        log("onStart", "file", file);
        super.onStart();
    }

    @Override
    public void onResume() {
        log("onResume", "file", file);
        super.onResume();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        log("setUserVisibleHint", "isVisibleToUser", isVisibleToUser);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        log("onSaveInstanceState", "file", file);
        super.onSaveInstanceState(outState);

        outState.putSerializable("file", file);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        log("onConfigurationChanged", "file", file);
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onPause() {
        log("onPause", "file", file);
        super.onPause();
    }

    @Override
    public void onStop() {
        log("onStop", "file", file);
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        log("onDestroyView", "file", file);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        log("onDestroy", "file", file);
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        log("onDetach", "file", file);
        super.onDetach();
    }
}
