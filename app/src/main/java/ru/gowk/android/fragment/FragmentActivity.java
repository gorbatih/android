package ru.gowk.android.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import ru.gowk.android.R;

/**
 * Created on Jul 01, 2016
 *
 * @author Vyacheslav Gorbatykh
 */
public class FragmentActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activity);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(new FFragmentPagerAdapter(getFragmentManager()));
    }

    private class FFragmentPagerAdapter extends FragmentPagerAdapter {

        FFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return 6;
        }

        @Override
        public Fragment getItem(int position) {
            if (position % 2 == 0) {
                return new Fragment1();
            }
            return new Fragment2();
        }
    }
}
