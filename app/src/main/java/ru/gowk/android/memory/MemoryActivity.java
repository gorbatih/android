package ru.gowk.android.memory;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import ru.gowk.android.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on Jul 14, 2016
 *
 * @author Vyacheslav Gorbatykh
 */
public class MemoryActivity extends Activity {

    private static List<int[]> q = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.memory);

//        final TextView textView = (TextView) findViewById(R.id.textView);
        Button button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Runtime runtime = Runtime.getRuntime();
                while (true) {
                    q.add(new int[1024 * 1024]);
                    double freeMemory = Math.round(((double) runtime.freeMemory()) / 1024);
                    double maxMemory = Math.round(((double) runtime.maxMemory()) / 1024);
                    double totalMemory = Math.round(((double) runtime.totalMemory()) / 1024);
                    Log.i("MemoryActivity", "mem (Kb): free=" + freeMemory + ", max=" + maxMemory
                            + ", total=" + totalMemory + ", added=" + q.size() + "Mb");
                }
            }
        });
    }
}
