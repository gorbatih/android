package ru.gowk.android;

import android.app.Application;
import com.google.inject.Injector;
import roboguice.RoboGuice;

/**
 * @author slavca
 * @since 13.05.2016
 */
public class App {
    public static Injector INJECTOR;

    public static void init(Application application){
        INJECTOR = RoboGuice.getOrCreateBaseApplicationInjector(application);
    }
}
