package ru.gowk.android.webview;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.InputEvent;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.*;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import ru.gowk.android.R;
import ru.gowk.android.sslsocketfactory.TlsSocketFactory;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created on Jun 03, 2016
 * @author Vyacheslav Gorbatykh
 */
public class WebViewActivity extends Activity {

    private CheckBox useCustomCheck;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);

        final EditText urlEdit = (EditText) findViewById(R.id.urlEdit);
        Button loadButton = (Button) findViewById(R.id.loadButton);
        final WebView webView = (WebView) findViewById(R.id.webview);
        useCustomCheck = (CheckBox) findViewById(R.id.useCustomCheck);


        webView.setWebViewClient(new WebViewClient() {
            private boolean useCustom;

            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                Uri uri = Uri.parse(url);
                if ((useCustom || useCustomCheck.isChecked()) && uri.getScheme().toLowerCase().equals("https")) {

                    Log.v("webview", "shouldInterceptRequest useCustom=" + useCustom + ", useCustomCheck.isChecked()=" + useCustomCheck.isChecked());

                    try {
                        HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
                        conn.setConnectTimeout(15000);
                        conn.setReadTimeout(15000);

                        if (conn instanceof HttpsURLConnection) {
                            ((HttpsURLConnection) conn).setSSLSocketFactory(new TlsSocketFactory());
                        }

                        conn.setRequestMethod("GET");

                        int responseCode = conn.getResponseCode();
                        if (responseCode == HttpURLConnection.HTTP_OK) {
                            return new WebResourceResponse("text/html", conn.getContentEncoding(), conn.getInputStream());
                        }
                    } catch (Exception e) {
                        Log.e("webview", "fail to load url using HttpsURLConnection", e);
                    }
                }
                return null;
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e("webview", "onReceivedError "+errorCode+", "+description+", "+failingUrl);
                super.onReceivedError(view, errorCode, description, failingUrl);

                if(errorCode == ERROR_FAILED_SSL_HANDSHAKE){
                    if (!useCustom) {
                        Log.v("webview", "onReceivedError try to reload");

                        useCustom = true;
                        view.loadUrl(failingUrl);
                    }
                }
            }
        });

        loadButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String url = urlEdit.getText().toString();
                Log.v("ssl-webview", "loadUrl: " + url);
                webView.loadUrl("about:blank");
                webView.loadUrl(url);
            }
        });

        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });
    }
}
