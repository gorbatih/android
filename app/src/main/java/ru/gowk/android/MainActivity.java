package ru.gowk.android;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import roboguice.adapter.IterableAdapter;
import ru.gowk.android.fragment.FragmentActivity;
import ru.gowk.android.googleservice.GoogleServiceActivity;
import ru.gowk.android.memory.MemoryActivity;
import ru.gowk.android.roboguice.RoboGuiceActivity;
import ru.gowk.android.sslsocketfactory.SslSocketFactoryActivity;
import ru.gowk.android.webview.WebViewActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on Jun 02, 2016
 * @author Vyacheslav Gorbatykh
 */
public class MainActivity extends ListActivity {
    private static final Map<String, Class<?extends Activity>> MENU_ITEM_MAP = new HashMap<>();
    static {
        MENU_ITEM_MAP.put("RoboGuice", RoboGuiceActivity.class);
        MENU_ITEM_MAP.put("SslSocketFactory", SslSocketFactoryActivity.class);
        MENU_ITEM_MAP.put("WebView", WebViewActivity.class);
        MENU_ITEM_MAP.put("GoogleService", GoogleServiceActivity.class);
        MENU_ITEM_MAP.put("Fragment", FragmentActivity.class);
        MENU_ITEM_MAP.put("Memory", MemoryActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ListAdapter listAdapter = new IterableAdapter<>(this, android.R.layout.simple_list_item_1, MENU_ITEM_MAP.keySet());
        setListAdapter(listAdapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        String menuItem = l.getItemAtPosition(position).toString();
        Class<? extends Activity> activityClass = MENU_ITEM_MAP.get(menuItem);
        Intent intent = new Intent(this, activityClass);
        startActivity(intent);
    }
}
